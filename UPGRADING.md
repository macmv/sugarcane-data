Upgrading this repository is where most of the work is for upgrading `bamboo`. It requires
a lot of manual intervention, because mojang changes things all time time. Also, this repo
needs to keep every old version present (including minor versions), so that past commits of
`bamboo` can still build.

As of writing, I am updating from 1.18.1 to 1.18.2. This means I might miss some details for
major version bumps. Once I upgrade to 1.19, I will include more details in this document.

The first thing to change is the static `VERSIONS` list in `main.rs`. Even if this is a minor
bump, a new entry must be added, so that we have all the versions that `bamboo` has depended
on in the past.

The next thing is `Version::from_num`. The argument is a major version, and that should map
to the latest minor version. So, in this function, you should replace the match for latest
with the new minor version. In my case, I replaced `18 => 1.18.1` with `18 => 1.18.2`.

Now, once these have been updated, try running the program. Make sure to not pass the
`--no-download` and `--no-extract` flags, as you will need to actually download a new
version.

In my case, everything worked, right up until the entity analyzer hit for 1.18.2. This
immediately crashed, with a nonsense looking error:
```
thread '<unnamed>' panicked at 'unknown class for entity Entity {
  id: 27,
  name: "falling_block",
  class: "",
  tags: [],
  category: "",
  width: 0.0,
  height: 0.0,
  fire_immune: false,
  tracking_range: 0,
  metadata: []
}',
src/analyzer/entity/new.rs:62:11
note: run with `RUST_BACKTRACE=1` environment variable to display a backtrace
```

So, if you get any errors, go fix them, and then run the extractor with `--webpage`,
and make sure everything looks correct. If it does, you're done! Push to `main`, and
let the CI update. Once the public page is done, you can go update `bamboo`.
