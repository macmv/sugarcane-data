use indicatif::{ProgressBar, ProgressStyle};
use std::{
  fs,
  fs::File,
  io,
  io::{Read, Write},
  path::Path,
};

/// Unzips the given file into the given directory.
pub fn unzip(filename: &Path, dir: &Path) -> io::Result<()> {
  info!("extracting to {}", dir.display());

  let file = File::open(filename)?;
  let mut archive = zip::ZipArchive::new(file).unwrap();

  for i in 0..archive.len() {
    let mut file = archive.by_index(i).unwrap();
    let out = match file.enclosed_name() {
      Some(path) => dir.join(path.to_owned()),
      None => continue,
    };

    {
      let comment = file.comment();
      if !comment.is_empty() {
        info!("File {} comment: {}", i, comment);
      }
    }

    if (&*file.name()).ends_with('/') {
      fs::create_dir_all(&out).unwrap();
    } else {
      if let Some(p) = out.parent() {
        if !p.exists() {
          fs::create_dir_all(&p).unwrap();
        }
      }
      let mut outfile = fs::File::create(&out).unwrap();
      io::copy(&mut file, &mut outfile).unwrap();
    }

    // Get and Set permissions
    #[cfg(unix)]
    {
      use std::os::unix::fs::PermissionsExt;

      if let Some(mode) = file.unix_mode() {
        fs::set_permissions(&out, fs::Permissions::from_mode(mode)).unwrap();
      }
    }
  }

  info!("done extracting to {}", dir.display());

  Ok(())
}

/// Returns `true` if the file was changed on disk. If there is already a file
/// on disk, and the size matches either the parameter `size`, or the metadata
/// from the remove host, then the download will be skipped.
pub fn download_file(url: &str, path: &Path, size: Option<u64>) -> io::Result<bool> {
  // Make sure the containing directory exists.
  if !path.parent().unwrap().exists() {
    return Err(io::Error::new(
      io::ErrorKind::NotFound,
      format!("the directory {} does not exist", path.parent().unwrap().display()),
    ));
  }
  // If we know the size beforehand, avoid making a request at all.
  if let Some(size) = size {
    if path.exists() {
      let file = File::open(path)?;
      if file.metadata()?.len() == size {
        return Ok(false);
      }
    }
  }

  let res =
    ureq::get(url).call().map_err(|e| io::Error::new(io::ErrorKind::InvalidData, e.to_string()))?;
  let total = res.header("Content-Length").and_then(|s| s.parse::<u64>().ok()).unwrap_or(0);
  let mut res = res.into_reader();
  if path.exists() {
    let file = File::open(path)?;
    if file.metadata()?.len() != total {
      info!(
        "file size differs from server and disk, redownloading {} (url: {})",
        path.display(),
        url
      );
    } else {
      info!("file size is the same on server and disk, skipping {} (url: {})", path.display(), url);
      return Ok(false);
    }
  }

  let mut file = File::create(path)?;
  let pb = ProgressBar::new(total);
  pb.set_style(
    ProgressStyle::default_bar()
      .template(
        "{msg}\n [{elapsed_precise} {eta_precise}] [{bar}] {bytes}/{total_bytes} {bytes_per_sec}",
      )
      .progress_chars("#-"),
  );
  pb.set_message(format!("downloading {} to {}", url, path.display()));

  // We don't want half a megabyte on the stack
  let chunk: &mut [u8] = &mut vec![0u8; 512 * 1024];
  let mut written = 0;
  loop {
    let len = res.read(chunk).unwrap();
    if len == 0 {
      break;
    }
    written += len;
    pb.set_position(written as u64);

    file.write_all(&chunk[..len])?;
  }
  pb.finish_with_message(format!("done downloading {} to {}", url, path.display()));
  Ok(true)
}
