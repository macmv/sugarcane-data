use super::{
  super::{block::BlockDef, Jar},
  Item, ItemDef,
};
use crate::decomp::{Instr, Op, Value};
use std::io;

pub fn process(jar: &mut Jar, blocks: &BlockDef) -> io::Result<ItemDef> {
  let class = jar.decomp("net/minecraft/item/Items", |name, _| name == "<clinit>");

  let (_, _, block) = class.funcs.first().unwrap();
  let items = generate(&block.instr, blocks, jar).map_err(|e| {
    io::Error::new(io::ErrorKind::Other, format!("error while parsing blocks: {}", e))
  })?;

  Ok(ItemDef { items })
}

fn generate(instr: &[Instr], blocks: &BlockDef, _jar: &mut Jar) -> io::Result<Vec<Item>> {
  let mut id = 0;
  let mut items = vec![];
  for i in instr {
    match i.clone() {
      Instr::SetField(field_name, it) => match it.unwrap_initial() {
        Value::StaticCall(_, name, args) => match name.as_str() {
          "register" => {
            match args.len() {
              // A single BlockItem argument
              1 => match &args[0].ops.borrow().get(0) {
                Some(Op::Call(_, name, args)) if name == "<init>" => {
                  match args[0].clone().unwrap_initial() {
                    Value::StaticField(class, _, name) => {
                      assert_eq!(class, "net/minecraft/block/Blocks");
                      let block = blocks.get_field(&name).unwrap();
                      items.push(Item {
                        id,
                        name: block.name.clone(),
                        places: Some(block.name.clone()),
                        class: "net/minecraft/item/BlockItem".into(),
                        ..Default::default()
                      });
                    }
                    _ => todo!(),
                  };
                }
                // No ops, so we just give up. Only happens on non-latest versions, so I don't
                // really care.
                None => {
                  items.push(Item {
                    id,
                    name: field_name.to_lowercase(),
                    class: "net/minecraft/item/BlockItem".into(),
                    ..Default::default()
                  });
                }
                arg => todo!("arg {arg:?}"),
              },
              // A block and category, or an item name and an item
              2 => match args[0].clone().unwrap_initial() {
                Value::StaticField(class, _, name) => {
                  assert_eq!(class, "net/minecraft/block/Blocks");
                  let block = blocks.get_field(&name).unwrap();
                  items.push(Item {
                    id,
                    name: block.name.clone(),
                    places: Some(block.name.clone()),
                    class: "net/minecraft/item/BlockItem".into(),
                    ..Default::default()
                  });
                }
                Value::String(name) => items.push(Item {
                  id,
                  name,
                  class: "net/minecraft/item/Item".into(),
                  ..Default::default()
                }),
                arg => todo!("arg {arg:?}"),
              },
              // A block item, which is dropped from multiple blocks
              // args[0] = block
              // args[1] = category
              // args[2] = all other blocks
              // TODO: Handle correctly.
              3 => match args[0].clone().unwrap_initial() {
                Value::StaticField(class, _, name) => {
                  assert_eq!(class, "net/minecraft/block/Blocks");
                  let block = blocks.get_field(&name).unwrap();
                  items.push(Item {
                    id,
                    name: block.name.clone(),
                    places: Some(block.name.clone()),
                    class: "net/minecraft/item/BlockItem".into(),
                    ..Default::default()
                  });
                }
                arg => todo!("arg {arg:?}"),
              },
              _ => todo!("args {args:?}"),
            }
            assert!(args.len() >= 1, "register takes 1 or more args, not {:?}", args);
            if !args[0].ops.borrow().is_empty() {
            } else {
            }
            id += 1;
          }
          _ => todo!("unknown item register name: {:?}", name),
        },
        v => todo!("unknown item {:?}", v),
      },
      _ => {}
    }
  }
  Ok(items)
}
