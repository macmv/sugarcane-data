use super::{block::BlockDef, tag::TagsDef, Jar};
use serde::Serialize;
use std::io;

mod new;
mod old;

#[derive(Debug, Clone, Serialize)]
pub struct ItemDef {
  items: Vec<Item>,
}

#[derive(Debug, Clone, Default, Serialize)]
pub struct Item {
  /// The id of the item.
  id:     u32,
  /// The name id, used everywhere imporant.
  name:   String,
  /// The block that this item will place.
  places: Option<String>,
  /// The full class of this item
  class:  String,

  tags: Vec<String>,
}

pub fn generate(jar: &mut Jar, blocks: &BlockDef, tags: &TagsDef) -> io::Result<impl Serialize> {
  if jar.ver().is_old() {
    old::process(jar, blocks)
  } else {
    let mut def = new::process(jar, blocks)?;
    for item in &mut def.items {
      item.tags = tags.get("item").get(&item.name);
    }
    Ok(def)
  }
}
