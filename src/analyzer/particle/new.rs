use super::{super::Jar, Particle, ParticleDef};
use crate::decomp::{Instr, Value};
use std::io;

pub fn process(jar: &mut Jar) -> io::Result<ParticleDef> {
  let class = jar.decomp("net/minecraft/particle/ParticleTypes", |name, _| name == "<clinit>");

  let (_, _, block) = class.funcs.first().unwrap();
  let particles = generate(&block.instr, jar).map_err(|e| {
    io::Error::new(io::ErrorKind::Other, format!("error while parsing blocks: {}", e))
  })?;

  Ok(ParticleDef { particles })
}

fn generate(instr: &[Instr], _jar: &mut Jar) -> io::Result<Vec<Particle>> {
  let mut id = 0;
  let mut particles = vec![];
  for i in instr {
    match i.clone() {
      Instr::SetField(_field_name, it) => {
        if !it.ops.borrow().is_empty() {
          continue;
        }
        match it.unwrap_initial() {
          Value::StaticCall(_, name, args) => match name.as_str() {
            "register" => {
              let particle_name = args[0].clone().unwrap_initial().unwrap_string();
              particles.push(Particle { name: particle_name, id });
              id += 1;
            }
            _ => todo!("unknown particle register name: {:?}", name),
          },
          v => todo!("unknown particle {:?}", v),
        }
      }
      _ => {}
    }
  }
  Ok(particles)
}
