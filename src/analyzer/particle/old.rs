use super::{super::Jar, Particle, ParticleDef};
use crate::decomp::{Instr, Op, Value};
use std::io;

pub fn process(jar: &mut Jar) -> io::Result<ParticleDef> {
  let class = jar.decomp("net/minecraft/util/EnumParticleTypes", |name, _| name == "<clinit>");

  let (_, _, block) = class.funcs.first().unwrap();
  let particles = generate(&block.instr, jar).map_err(|e| {
    io::Error::new(io::ErrorKind::Other, format!("error while parsing blocks: {}", e))
  })?;

  Ok(ParticleDef { particles })
}

fn generate(instr: &[Instr], _jar: &mut Jar) -> io::Result<Vec<Particle>> {
  let mut particles = vec![];
  for i in instr {
    match i.clone() {
      Instr::SetField(_name, it) => {
        if !matches!(it.initial, Value::Class(_)) {
          continue;
        }
        match &it.ops.borrow()[0] {
          Op::Call(_, name, args) => match name.as_str() {
            "<init>" => {
              let _enum_name = &args[0];
              let _variant = &args[1];
              let particle_name = args[2].clone().unwrap_initial().unwrap_string();
              let id = args[3].clone().unwrap_initial().unwrap_int();
              let _should_ignore_range = &args[4];
              particles
                .insert(id as usize, Particle { name: particle_name, id: id.try_into().unwrap() });
            }
            _ => todo!("unknown particle register name: {:?}", name),
          },
          v => todo!("unknown particle {:?}", v),
        }
      }
      _ => {}
    }
  }
  Ok(particles)
}
