use super::Jar;
use serde::Serialize;
use std::io;

mod new;
mod old;

#[derive(Debug, Clone, Serialize)]
pub struct Particle {
  name: String,
  id:   u32,
}

#[derive(Debug, Clone, Serialize)]
pub struct ParticleDef {
  particles: Vec<Particle>,
}

pub fn generate(jar: &mut Jar) -> io::Result<impl Serialize> {
  if jar.ver().is_old() {
    old::process(jar)
  } else {
    new::process(jar)
  }
}
