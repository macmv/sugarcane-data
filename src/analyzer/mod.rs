use super::Version;
use std::{
  collections::HashMap,
  fs,
  fs::File,
  io,
  io::Write,
  path::{Path, PathBuf},
  time::Instant,
};

mod block;
mod convert;
mod entity;
mod item;
mod jar;
mod packet;
mod particle;
mod tag;

use convert::Converter;
use jar::Jar;

pub struct Output {
  base:  PathBuf,
  files: HashMap<&'static str, String>,
}

/// Analyzes a given path. This should be something like `build/mcp-08`. This
/// function will create an output folder next to `mc-{ver}/src`. It will look
/// for various files within `mc-{ver}/src` to perform the analysis.
pub fn analyze(b: &Path, ver: Version, pretty: bool) -> io::Result<Output> {
  let mut files = HashMap::new();

  let p = b.join(ver.build_dir());
  fs::create_dir_all(p.join("generated"))?;

  let time = Instant::now();
  info!("generating output json for {}", p.display());

  let file = File::open(p.join("mc/mc.jar"))?;
  let mut jar = Jar::new(file, ver, &p);

  macro_rules! generate {
    ($m:ident, $name:expr $(, $arg:expr)*) => {{
      let def = $m::generate(&mut jar $(,$arg)*)?;
      files.insert($name, if pretty {
        serde_json::to_string_pretty(&def)?
      } else {
        serde_json::to_string(&def)?
      });
      def
    }};
  }

  generate!(packet, "protocol");
  generate!(particle, "particles");
  let tags = generate!(tag, "tags");
  let blocks = generate!(block, "blocks", &tags);
  generate!(item, "items", &blocks, &tags);
  generate!(entity, "entities", &tags);

  info!(
    "generated output json for {} (took {:?})",
    p.display(),
    Instant::now().duration_since(time)
  );
  Ok(Output { base: b.join(ver.build_dir()), files })
}

impl Output {
  pub fn to_disk(&self) -> io::Result<()> {
    for (name, data) in &self.files {
      let path = &self.base.join(format!("generated/{name}.json"));
      let mut file = File::create(path)?;
      file.write_all(data.as_bytes())?;
    }
    Ok(())
  }
}
