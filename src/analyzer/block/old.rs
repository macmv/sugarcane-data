use super::{Block, BlockDef, Jar};
use crate::decomp::{Instr, Item, Op, Value, Vars};
use noak::error::DecodeError;
use std::io;

/// Easy, simple block registering that everyone liked and should not have been
/// removed.
pub fn generate(jar: &mut Jar) -> io::Result<BlockDef> {
  let class = jar.decomp("net/minecraft/block/Block", |name, _| name == "registerBlocks");

  let (_, _, block) = class.funcs.first().unwrap();
  let blocks = process_old(&block.instr, jar).map_err(|e| {
    io::Error::new(io::ErrorKind::Other, format!("error while parsing blocks: {}", e))
  })?;

  Ok(BlockDef { blocks })
}

fn process_old(instr: &[Instr], jar: &mut Jar) -> Result<Vec<Block>, DecodeError> {
  let mut out = vec![];
  let mut vars = Vars::new(&[]);
  for i in instr {
    match i {
      Instr::Expr(e) => {
        match &e.initial {
          Value::StaticCall(_, name, args) if name == "registerBlock" => {
            let id = args[0].clone();
            let name = args[1].clone();
            let value = args[2].clone();
            let name = match name.unwrap_initial() {
              Value::String(v) => v,
              Value::StaticField(_, _, name) => match name.as_str() {
                "AIR_ID" => "air".into(),
                v => panic!("unexpected static {:?}", v),
              },
              v => panic!("unexpected {:?}", v),
            };
            let value = match value.initial {
              Value::Class(ref _name) => value,
              Value::Var(id) => vars.load_mut(id).clone(),
              v => panic!("unexpected {:?}", v),
            };
            let class = if let Value::Class(ref name) = value.initial {
              name.clone()
            } else {
              panic!("unexpected {:?}", value)
            };
            let mut b = Block {
              id: id.clone().unwrap_initial().unwrap_int().try_into().unwrap(),
              name,
              class,
              ..Default::default()
            };
            // Call first, as the constructor is run before functions on the block.
            add_props_for_block(&mut b, jar)?;
            add_props(&mut b, value);
            out.push(b);
          }
          // This is some nonsense at the end of the static block that we can ignore.
          Value::StaticField(class, _ty, name)
            if class == "net/minecraft/block/Block"
              && (name == "blockRegistry" || name == "REGISTRY") => {}
          _ => todo!("{:?}", e),
        }
      }
      Instr::Assign(var, value) => {
        vars.store(*var, value.clone());
      }
      _ => {}
    }
  }
  Ok(out)
}

/// Reads the given value, and parses what those functions mean. This parses
/// something like `new BlockStone().setResistance(10.0).setHardness(5)`. For
/// each call it finds, it will override whatever is already stored in the block
/// for that property.
fn add_props(b: &mut Block, value: Item) {
  // if b.name == "cobblestone" {
  //   dbg!(&value);
  // }
  for op in value.ops.borrow().clone().into_iter() {
    match op {
      Op::Call(_, name, args) => match name.as_str() {
        "setResistance" => {
          assert_eq!(args.len(), 1, "unexpected args for setResistance");
          b.resistance = args[0].clone().unwrap_initial().unwrap_float()
        }
        "setHardness" => {
          assert_eq!(args.len(), 1, "unexpected args for setHardness");
          b.hardness = args[0].clone().unwrap_initial().unwrap_float()
        }
        "setUnlocalizedName" => {
          assert_eq!(args.len(), 1, "unexpected args for setUnlocalizedName");
          b.unlocalized_name = args[0].clone().unwrap_initial().unwrap_string()
        }
        _ => {}
      },
      _ => {}
    }
  }
}

/// This uses the class of the given block, and reads that from the zip archive,
/// and reads the rest of the properties from the constructor. This includes
/// block material, more properties, etc.
///
/// This uses the `class` property of the block for the class to read. It will
/// then override any existing properties with any calls it finds in the
/// constructor.
fn add_props_for_block(b: &mut Block, jar: &mut Jar) -> Result<(), DecodeError> {
  let class = jar.decomp(&b.class, |name, _| name == "<init>");
  let (_, _, block) = class.funcs.first().unwrap();

  for i in &block.instr {
    match i {
      Instr::Super(args) => {
        if !args.is_empty() {
          match args[0].clone().initial {
            Value::StaticField(class, _ty, val) => {
              if class == "net/minecraft/block/material/Material" {
                b.material = val;
              }
            }
            _ => {}
          }
        }
      }
      _ => {}
    }
  }

  Ok(())
}
