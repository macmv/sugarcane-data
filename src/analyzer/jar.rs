use super::{Converter, Version};
use crate::decomp;
use noak::{reader::Class, AccessFlags};
use std::{collections::HashMap, fs::File, io::Read, path::Path};
use zip::ZipArchive;

pub struct Jar {
  convert: Converter,
  classes: ClassFiles,
  ver:     Version,

  super_classes: HashMap<String, String>,
  implements:    HashMap<String, Vec<String>>,
  // Map of class names to function names to decompiled functions.
  decomp_funcs:  HashMap<String, HashMap<String, (decomp::Descriptor, decomp::VarBlock)>>,
}

pub struct ClassFiles {
  /// Obfuscated class name to class files. This is a live cache, so any time we
  /// lookup a class file, it might need to be loaded from the zip file, and
  /// then added here.
  classes: HashMap<String, Vec<u8>>,
  zip:     ZipArchive<File>,
}

impl Jar {
  pub fn new(file: File, ver: Version, path: &Path) -> Self {
    let mut classes = ClassFiles::new(file);
    Jar {
      convert: if ver.is_old() {
        Converter::new_mcp(path)
      } else {
        Converter::new_yarn(path, &mut classes)
      },
      classes,
      ver,
      super_classes: HashMap::new(),
      implements: HashMap::new(),
      decomp_funcs: HashMap::new(),
    }
  }

  pub fn zip(&mut self) -> &mut ZipArchive<File> {
    &mut self.classes.zip
  }

  pub fn ver(&self) -> Version {
    self.ver
  }

  /// Returns the decompiled class for the given deobfuscated class name.
  #[track_caller]
  pub fn decomp(
    &mut self,
    p: &str,
    is_valid: impl Fn(&str, &decomp::Descriptor) -> bool,
  ) -> decomp::Class {
    let file = self.classes.class_file(&self.convert.class_rev(p)).to_vec();
    decomp::class(self, &file, |name, desc, flags| {
      !flags.contains(AccessFlags::PRIVATE) && is_valid(name, desc)
    })
    .unwrap_or_else(|e| panic!("failed to decompile class {}: {:?}", p, e))
  }
  /// Returns the decompiled class for the given obfuscated class name.
  #[track_caller]
  pub fn decomp_obf(
    &mut self,
    p: &str,
    is_valid: impl Fn(&str, &decomp::Descriptor) -> bool,
  ) -> decomp::Class {
    let file = self.classes.class_file(p).to_vec();
    match decomp::class(self, &file, |name, desc, flags| {
      !flags.contains(AccessFlags::PRIVATE) && is_valid(name, desc)
    }) {
      Ok(v) => v,
      Err(e) => panic!("failed to decompile class {}: {:?}", p, e),
    }
  }

  /// Decompiles the given function on that class. This will cache decompiled
  /// functions, so it should be used when possible.
  #[track_caller]
  pub fn decomp_func(&mut self, p: &str, func: &str) -> &(decomp::Descriptor, decomp::VarBlock) {
    if let Some(true) = self.decomp_funcs.get(p).map(|f| f.contains_key(func)) {
      &self.decomp_funcs[p][func]
    } else {
      let mut class = self.decomp(p, |name, _| name == func);
      if class.funcs.is_empty() {
        panic!("no function named {}#{}", p, func);
      }
      // Find the function with the most arguments. Not a "good" solution, but it
      // works.
      if class.funcs.len() != 1 {
        class.funcs.sort_unstable_by(|(_, a, _), (_, b, _)| a.args.len().cmp(&b.args.len()));
      }
      let (_, desc, instr) = class.funcs.pop().unwrap();
      self
        .decomp_funcs
        .entry(p.into())
        .or_insert_with(HashMap::new)
        .insert(func.into(), (desc, instr));
      &self.decomp_funcs[p][func]
    }
  }

  /// Decompiles the given function on that class. This will cache decompiled
  /// functions, so it should be used when possible.
  pub fn decomp_func_opt(
    &mut self,
    p: &str,
    func: &str,
  ) -> Option<&(decomp::Descriptor, decomp::VarBlock)> {
    if let Some(true) = self.decomp_funcs.get(p).map(|f| f.contains_key(func)) {
      Some(&self.decomp_funcs[p][func])
    } else {
      let mut class = self.decomp(p, |name, _| name == func);
      if class.funcs.is_empty() {
        return None;
      }
      // Find the function with the most arguments. Not a "good" solution, but it
      // works.
      if class.funcs.len() != 1 {
        class.funcs.sort_unstable_by(|(_, a, _), (_, b, _)| a.args.len().cmp(&b.args.len()));
      }
      let (_, desc, instr) = class.funcs.pop().unwrap();
      self
        .decomp_funcs
        .entry(p.into())
        .or_insert_with(HashMap::new)
        .insert(func.into(), (desc, instr));
      Some(&self.decomp_funcs[p][func])
    }
  }

  /// Returns the obfuscated superclass of the given obfuscated class.
  pub fn obf_super(&mut self, c: &str) -> String {
    if let Some(sup) = self.super_classes.get(c) {
      sup.clone()
    } else {
      let mut class = Class::new(self.classes.class_file(c)).unwrap();
      let sup: String = class.super_class_name().unwrap().unwrap().to_str().unwrap().into();
      self.super_classes.insert(c.into(), sup.clone());
      sup
    }
  }

  /// Returns the deobfuscated superclass of the given deobfuscated class.
  pub fn deobf_super(&mut self, deobf: &str) -> String {
    let obf = self.convert.class_rev(deobf);
    let obf_sup = self.obf_super(&obf);
    if self.convert.has_class(&obf_sup) {
      self.convert.class(&obf_sup).name().into()
    } else {
      obf_sup
    }
  }

  /// Returns a list of all the interfaces this class implements.
  pub fn obf_implements(&mut self, c: &str) -> Vec<String> {
    if let Some(interfaces) = self.implements.get(c) {
      interfaces.clone()
    } else {
      let mut class = Class::new(self.classes.class_file(c)).unwrap();
      let interfaces: Vec<_> = class
        .interfaces()
        .unwrap()
        .map(|i| class.pool().unwrap().retrieve(i.unwrap()).unwrap().name.to_str().unwrap().into())
        .collect();
      self.implements.insert(c.into(), interfaces.clone());
      interfaces
    }
  }

  /// Returns the deobfuscated list of interfaces the given deobfuscated class
  /// implements.
  pub fn deobf_implements(&mut self, deobf: &str) -> Vec<String> {
    let obf = self.convert.class_rev(deobf);
    let obf_impls = self.obf_implements(&obf);
    obf_impls
      .into_iter()
      .map(|obf_imp| {
        if self.convert.has_class(&obf_imp) {
          self.convert.class(&obf_imp).name().into()
        } else {
          obf_imp
        }
      })
      .collect()
  }
}

impl decomp::Converter for Jar {
  fn has_class_file(&mut self, name: &str) -> bool {
    if self.convert.has_class_rev(name) {
      self.classes.has_class(&self.convert.class_rev(name))
    } else {
      false
    }
  }
  #[track_caller]
  fn class_file(&mut self, name: &str) -> &[u8] {
    self.classes.class_file(&self.convert.class_rev(name))
  }
  fn class(&self, name: &str) -> String {
    if self.convert.has_class(name) {
      self.convert.class(name).name().into()
    } else {
      name.into()
    }
  }
  fn class_rev(&self, name: &str) -> String {
    self.convert.class_rev(name)
  }

  #[track_caller]
  fn field(&mut self, class: &str, field: &str) -> String {
    // The classes/interfaces we are currently scanning for fields
    let mut c: Vec<String> = vec![class.into()];
    loop {
      let mut new_c = vec![];
      for c in &c {
        // We don't want to check for superclasses on unknown classes
        if !self.convert.has_class(c) {
          continue;
        }
        if self.convert.class(c).has_field(field) {
          return self.convert.class(c).field(field).unwrap().into();
        }
        let sup = self.obf_super(c);
        if sup != "java/lang/Object" && sup != "java/lang/Enum" {
          new_c.push(sup);
        }
        for i in self.obf_implements(c) {
          new_c.push(i);
        }
      }
      if new_c.is_empty() {
        // warn!(
        //   "could not find field {} for class {}",
        //   func,
        //   self.convert.class(&class).name()
        // );
        return field.into();
      }
      c = new_c;
    }
  }

  #[track_caller]
  fn func(&mut self, class: &str, func: &str, desc: &str) -> String {
    // The classes/interfaces we are currently scanning for functions
    let mut c: Vec<String> = vec![class.into()];
    loop {
      let mut new_c = vec![];
      for c in &c {
        // We don't want to check for superclasses on unknown classes
        if !self.convert.has_class(c) {
          continue;
        }
        if self.convert.class(c).has_func(func, desc) {
          return self.convert.class(c).func(func, desc).unwrap().into();
        }
        let sup = self.obf_super(c);
        if sup != "java/lang/Object" {
          new_c.push(sup);
        }
        for i in self.obf_implements(c) {
          new_c.push(i);
        }
      }
      if new_c.is_empty() {
        // warn!(
        //   "could not find func {} for class {}",
        //   func,
        //   self.convert.class(&class).name()
        // );
        return func.into();
      }
      c = new_c;
    }
  }
}

impl ClassFiles {
  pub fn new(file: File) -> Self {
    ClassFiles {
      classes: HashMap::new(),
      zip:     ZipArchive::new(file).expect("invalid zip archive"),
    }
  }

  /// Returns true if there is a class file for the given class name (not path).
  pub fn has_class(&mut self, p: &str) -> bool {
    if self.classes.contains_key(p) {
      true
    } else {
      self.zip.by_name(&format!("{}.class", p)).is_ok()
    }
  }
  /// Returns the class file for the given class name.
  #[track_caller]
  pub fn class_file(&mut self, p: &str) -> &[u8] {
    if self.classes.contains_key(p) {
      return &self.classes[p];
    }
    let mut f = self.zip.by_name(&format!("{}.class", p)).unwrap();
    let mut bytes = vec![];
    f.read_to_end(&mut bytes).unwrap();
    self.classes.insert(p.into(), bytes);
    &self.classes[p]
  }

  pub fn class_names(&self) -> Vec<String> {
    self.zip.file_names().filter_map(|s| s.strip_suffix(".class").map(|s| s.into())).collect()
  }
}
