//! This is the first pass of the parser. This reads bytecode into an abstract
//! syntax tree of sorts, that can be used to generate a packet reader. With
//! some expansion, this could also be used to generate java source code.

use super::{Field, Jar};
use crate::{decomp, Version};
use noak::error::DecodeError;

pub fn pass(
  p: &str,
  jar: &mut Jar,
) -> Result<(String, String, String, Vec<Field>, decomp::VarBlock), DecodeError> {
  let ver = jar.ver();
  let mut class = jar.decomp_obf(p, |name, desc| {
    if ver.is_old() {
      name == "readPacketData"
    } else if ver < Version::new(17, 1) {
      name == "read"
    } else {
      desc.args == vec![decomp::Type::Class("net/minecraft/network/PacketByteBuf".into())]
        && (name == "<init>" || name == "read")
    }
  });

  assert_eq!(class.funcs.len(), 1, "should have only returned one reader function");

  Ok((
    clean_name(&class.name, jar.ver()),
    class.name,
    class.extends,
    class.fields.into_iter().map(|(name, ty)| Field { name, ty }).collect(),
    class.funcs.pop().unwrap().2,
  ))
}

fn clean_name(name: &str, ver: Version) -> String {
  normalize_name(
    if ver.maj() < 14 {
      // 1.8 packet, which looks like this: `S0EPacketSpawnObject`
      // or, a 1.9+ packet, which looks like this: `SPacketSpawnObject`
      // or, in rare cases, the packet name is just `Position` or similar.
      name.split("Packet").nth(1).unwrap_or(name)
    } else {
      // 1.14+ packet, which looks like this: `SpawnObjectS2CPacket`
      name.split("S2C").next().unwrap().split("C2S").next().unwrap()
    },
    ver,
  )
}

fn normalize_name(name: &str, ver: Version) -> String {
  let name = if ver.maj() < 14 {
    name.into()
  } else {
    // This finds the first capital, going right to left. If the capital is at zero,
    // this is a single word. Otherwise, we take the last word, move it to the start
    // of the string, and then match against that. This helps with a bunch of names,
    // such as `SpawnExperienceOrb` being renamed to `ExperienceOrbSpawn` in 1.14.
    let mut i = name.bytes().rev().position(|c: u8| (c as char).is_uppercase()).unwrap();
    i = name.len() - i;
    i -= 1;
    if i != 0 && name.contains("Spawn") {
      let first = &name[..i];
      let second = &name[i..];
      let mut s = String::with_capacity(first.len() + second.len());
      s += second;
      s += first;
      s
    } else {
      name.into()
    }
  };
  #[rustfmt::skip]
  let s = match name.as_str() {
    // Clientbound 1.12/1.14 packets. The resulting packet is either a 1.12 packet
    // name, or a 1.14 name, whichever I see fit.
    "SpawnEntity"                     => "SpawnObject",
    "GlobalEntitySpawn"               => "SpawnGlobalEntity",
    "EntityAnimation"                 => "Animation",
    "BlockBreakingProgress"           => "BlockBreakAnim",
    "BlockEntityUpdate"               => "UpdateTileEntity",
    "BlockChange"                     => "BlockUpdate",
    "UpdateBossInfo"                  => "BossBar",
    "ServerDifficulty"                => "Difficulty",
    "ChatMessage" | "GameMessage"     => "Chat",
    "ChunkDeltaUpdate"                => "MultiBlockChange",
    "ConfirmGuiAction"
      | "ConfirmScreenAction"         => "ConfirmTransaction",
    "CloseContainer"                  => "CloseWindow",
    "OpenContainer"                   => "OpenWindow",
    "Inventory"                       => "WindowItems",
    "ContainerSlotUpdate"
      | "ScreenHandlerSlotUpdate"     => "SetSlot",
    "ContainerPropertyUpdate"
      | "ScreenHandlerPropertyUpdate" => "WindowProperty",
    "CooldownUpdate"                  => "Cooldown",
    "PlaySoundId"                     => "CustomSound",
    "GameStateChange"                 => "ChangeGameState",
    "WorldEvent"                      => "Effect",
    "Particles"                       => "Particle",
    "GameJoin"                        => "JoinGame",
    "MapUpdate"                       => "Maps",
    "MoveRelative"                    => "EntityRelMove",
    "RotateAndMoveRelative"           => "EntityLookMove",
    "Rotate"                          => "EntityLook",
    "MoveVehicle"                     => "VehicleMove",
    "PlayerListItem"                  => "PlayerList",
    "PlayerPositionLook"              => "PlayerPosLook",
    "EntitiesDestroy"                 => "DestroyEntities",
    "RemoveEntityStatusEffect"        => "RemoveEntityEffect",
    "PlayerRespawn"                   => "Respawn",
    "EntitySetHeadYaw"                => "EntityHeadLook",
    "SetCameraEntity"                 => "Camera",
    "DisplayObjective"
      | "DisplayScoreboard"           => "ScoreboardDisplay",
    "EntityTrackerUpdate"             => "EntityMetadata",
    "EntityVelocityUpdate"            => "EntityVelocity",
    "EntityEquipmentUpdate"           => "EntityEquipment",
    "ExperienceBarUpdate"             => "SetExperience",
    "HealthUpdate"                    => "UpdateHealth",
    "ScoreboardObjectiveUpdate"       => "ScoreboardObjective",
    "EntityPassengersSet"             => "SetPassengers",
    "Team"                            => "Teams",
    "ScoreboardPlayerUpdate"          => "UpdateScore",
    "PositionPlayerSpawn"             => "SpawnPosition",
    "WorldTimeUpdate"                 => "TimeUpdate",
    "SoundEffect"                     => "PlaySound",
    "PlayerListHeaderFooter"          => "PlayerListHeader",
    "ItemPickupAnimation"             => "CollectItem",
    "EntityPosition"                  => "EntityTeleport",
    "AdvancementUpdate"               => "AdvancementInfo",
    "EntityAttributes"                => "EntityProperties",
    "EntityStatusEffect"              => "EntityEffect",

    // Serverbound packets
    "TeleportConfirm"           => "ConfirmTeleport",
    // "ConfirmGuiAction"       => "ConfirmTransaction", (already listed above)
    "EnchantItem"               => "ButtonClick",
    "GuiClose"                  => "CloseWindow",
    "PlayerInteractEntity"      => "UseEntity",
    // Fabric needs to choose a name and stick with it. The latest change
    // (adding *AndOnGround) was for 1.17. This is completely uneeded.
    "PlayerMove"
      | "OnGroundOnly"          => "PlayerOnGround",
    "PositionOnly"
      | "Position"
      | "PositionAndOnGround"   => "PlayerPosition",
    "Both"
      | "PositionRotation"
      | "Full"                  => "PlayerPositionRotation",
    "LookOnly"
      | "Rotation"
      | "LookAndOnGround"       => "PlayerRotation",
    "BoatPaddleState"           => "SteerBoat",
    // EntityAction (mcp), or ClientCommand (fabric) is used when sneaking/sprinting
    "EntityAction"
      | "ClientCommand"         => "PlayerCommand",
    // PlayerDigging (mcp), or PlayerAction (fabric) is used when digging a block
    "PlayerDigging"
      | "PlayerAction"          => "PlayerDig",
    "Input"                     => "PlayerInput",
    "UpdateSelectedSlot"        => "HeldItemChange",
    "HandSwing"                 => "Animation",
    "SpectatorTeleport"         => "Spectate",
    "PlayerTryUseItemOnBlock"   => "PlayerInteractBlock",
    "PlayerTryUseItem"          => "PlayerInteractItem",
    _ => return name,
  };
  s.into()
}
