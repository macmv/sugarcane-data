use super::Version;
use crate::decomp;
use noak::{
  error::DecodeError,
  reader::{
    attributes::{AttributeContent, RawInstruction},
    cpool::{value::FieldRef, Item},
    Class,
  },
};
use std::{fs, path::Path};

/// Reading static fields is difficult, so this is the hardcoded solution. See
/// [`lookup_field_int`] for my attempt at reading field values.
pub fn get_field_int(class: &str, field: &str, ver: Version) -> i32 {
  match class {
    "net/minecraft/entity/item/EntityPainting$EnumArt" => "SkullAndRoses".len() as i32,
    "net/minecraft/world/WorldType" => 0,
    "net/minecraft/block/Block" => 0,
    "net/minecraft/util/EnumParticleTypes" => 0,
    "net/minecraft/util/SoundEvent" => 0,
    "net/minecraft/util/registry/Registry" => 0,
    "net/minecraft/item/ItemStack" => 0,
    "net/minecraft/particle/ParticleTypes" => 0,
    "net/minecraft/world/level/LevelGeneratorType" => 0,
    "net/minecraft/world/biome/source/BiomeArray" => 0,
    "net/minecraft/util/registry/DynamicRegistryManager$Impl" => 0,
    "net/minecraft/world/dimension/DimensionType" => 0,
    "net/minecraft/block/entity/JigsawBlockEntity$Joint" => 0,
    "net/minecraft/text/LiteralText" => 0,
    class @ "net/minecraft/scoreboard/ServerScoreboard$UpdateMode" => match field {
      "REMOVE" => 1,
      field => unreachable!("failed to lookup field for class {} (field {})", class, field),
    },
    class @ "net/minecraft/scoreboard/ScoreboardCriterion$RenderType" => match field {
      "INTEGER" => 0,
      field => unreachable!("failed to lookup field for class {} (field {})", class, field),
    },
    s if s.starts_with("net/minecraft/network/play/") => {
      match &s["net/minecraft/network/play/".len()..] {
        "server/S38PacketPlayerListItem$1" | "server/SPacketPlayerListItem$1" => 0,
        "server/S44PacketWorldBorder$1" | "server/SPacketWorldBorder$1" => 0,
        "server/SPacketUpdateBossInfo$1" => 0,
        class @ "server/S3CPacketUpdateScore$Action"
        | class @ "server/SPacketUpdateScore$Action" => match field {
          "REMOVE" => 1,
          field => unreachable!("failed to lookup field for class {} (field {})", class, field),
        },
        class @ "server/S42PacketCombatEvent$Event" | class @ "server/SPacketCombatEvent$Event" => {
          match field {
            "ENTER_COMBAT" => 0,
            "END_COMBAT" => 1,
            "ENTITY_DIED" => 2,
            field => unreachable!("failed to lookup field for class {} (field {})", class, field),
          }
        }
        class @ "server/S44PacketWorldBorder$Action" => match field {
          "SET_SIZE" => 0,
          "LERP_SIZE" => 1,
          "SET_CENTER" => 2,
          "INITIALIZE" => 3,
          "SET_WARNING_TIME" => 4,
          "SET_WARNING_BLOCKS" => 5,
          field => unreachable!("failed to lookup field for class {} (field {})", class, field),
        },
        class @ "server/S45PacketTitle$Type" | class @ "server/SPacketTitle$Type" => {
          if ver.maj() >= 11 {
            match field {
              "TITLE" => 0,
              "SUBTITLE" => 1,
              "ACTIONBAR" => 2,
              "TIMES" => 3,
              "CLEAR" => 4,
              "RESET" => 5,
              field => unreachable!("failed to lookup field for class {} (field {})", class, field),
            }
          } else {
            match field {
              "TITLE" => 0,
              "SUBTITLE" => 1,
              "TIMES" => 2,
              "CLEAR" => 3,
              "RESET" => 4,
              field => unreachable!("failed to lookup field for class {} (field {})", class, field),
            }
          }
        }
        class @ "server/SPacketRecipeBook$State" => match field {
          "INIT" => 0,
          "ADD" => 1,
          "REMOVE" => 2,
          field => unreachable!("failed to lookup field for class {} (field {})", class, field),
        },
        class @ "client/C02PacketUseEntity$Action" | class @ "client/CPacketUseEntity$Action" => {
          match field {
            "INTERACT" => 0,
            "ATTACK" => 1,
            "INTERACT_AT" => 2,
            field => unreachable!("failed to lookup field for class {} (field {})", class, field),
          }
        }
        class @ "client/CPacketRecipeInfo$Purpose" => match field {
          "SHOWN" => 0,
          "SETTINGS" => 1,
          field => unreachable!("failed to lookup field for class {} (field {})", class, field),
        },
        class @ "client/CPacketSeenAdvancements$Action" => match field {
          "OPENED_TAB" => 0,
          "CLOSED_SCREEN" => 1,
          field => unreachable!("failed to lookup field for class {} (field {})", class, field),
        },
        class => unreachable!("failed to lookup field for class {}", class),
      }
    }
    s if s.starts_with("net/minecraft/network/packet") => {
      match &s["net/minecraft/network/packet/".len()..] {
        "s2c/play/BossBarS2CPacket" => 0,
        "s2c/play/PlayerListS2CPacket" => 0,
        "s2c/play/WorldBorderS2CPacket" => 0,
        "s2c/query/QueryResponseS2CPacket" => 0,
        class @ "s2c/play/CombatEventS2CPacket$Type" => match field {
          "ENTER_COMBAT" => 0,
          "END_COMBAT" => 1,
          "ENTITY_DIED" => 2,
          field => unreachable!("failed to lookup field for class {} (field {})", class, field),
        },
        class @ "s2c/play/UnlockRecipesS2CPacket$Action" => match field {
          "INIT" => 0,
          "ADD" => 1,
          "REMOVE" => 2,
          field => unreachable!("failed to lookup field for class {} (field {})", class, field),
        },
        class @ "s2c/play/TitleS2CPacket$Action" => match field {
          "TITLE" => 0,
          "SUBTITLE" => 1,
          "ACTIONBAR" => 2,
          "TIMES" => 3,
          "CLEAR" => 4,
          "RESET" => 5,
          field => unreachable!("failed to lookup field for class {} (field {})", class, field),
        },
        class @ "c2s/play/PlayerInteractEntityC2SPacket$InteractionType" => match field {
          "INTERACT" => 0,
          "ATTACK" => 1,
          "INTERACT_AT" => 2,
          field => unreachable!("failed to lookup field for class {} (field {})", class, field),
        },
        class @ "c2s/play/RecipeBookDataC2SPacket$Mode" => match field {
          "SHOWN" => 0,
          "SETTINGS" => 1,
          field => unreachable!("failed to lookup field for class {} (field {})", class, field),
        },
        class @ "c2s/play/AdvancementTabC2SPacket$Action" => match field {
          "OPENED_TAB" => 0,
          "CLOSED_SCREEN" => 1,
          field => unreachable!("failed to lookup field for class {} (field {})", class, field),
        },
        class @ "s2c/play/GameStateChangeS2CPacket$Reason" => match field {
          "a" => 0,
          field => unreachable!("failed to lookup field for class {} (field {})", class, field),
        },
        class => unreachable!("failed to lookup field for class {}", class),
      }
    }
    class => unreachable!("failed to lookup field for class {}", class),
  }
}

#[allow(dead_code)]
pub fn lookup_field_int(
  _convert: &dyn decomp::Converter,
  p: &Path,
  f: FieldRef,
) -> Result<i32, DecodeError> {
  dbg!(&f);
  let name = f.class.name.to_str().unwrap();
  let bytes =
    fs::read(p.join(format!("{}.class", name))).unwrap_or_else(|_| panic!("no file at {}", name));
  let mut class = Class::new(&bytes)?;
  debug!("class: {}", class.this_class_name()?.to_str().unwrap());
  let mut index = None;
  for field in class.fields()? {
    let field = field?;
    let pool = class.pool()?;
    if pool.retrieve(field.name())? == f.name_and_type.name {
      index = Some(field.name());
      break;
    }
  }
  if index.is_some() {
    for method in class.methods()? {
      let method = method?;
      let pool = class.pool()?;
      if pool.retrieve(method.name())?.to_str().unwrap() == "<init>" {
        debug!("FOUND INIT");
        for a in method.attributes() {
          if let AttributeContent::Code(code) = a?.read_content(pool)? {
            for instr in code.raw_instructions() {
              let (_i, instr) = instr?;
              debug!("got {:?}", instr);
              if let RawInstruction::InvokeSpecial { index } = instr {
                match pool.get(index)? {
                  Item::MethodRef(_method) => {
                    /*
                    let name_and_type = pool.retrieve(method.name_and_type)?;
                    let name = convert.method(name_and_type.name.to_str().unwrap());
                    debug!("{}", name);
                    */
                  }
                  item => debug!("got item {:?}", item),
                }
                debug!("name: {}", name);
              }
            }
          }
        }
      }
    }
  }
  Ok(0)
}
