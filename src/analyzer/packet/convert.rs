//! This is the second pass of the analyzer. It takes the java abstract syntax
//! tree, and converts that into a packet reader that is very easy to convert to
//! Rust. It could also be converted into other languages, but it is targeted at
//! Rust for now.

use super::{Cond, Expr, Instr, Lit, Op, Range, Value, VarBlock, VarKind, Version};
use crate::decomp;

pub fn pass(needs_super: bool, v: &decomp::VarBlock, ver: Version) -> VarBlock {
  let block = pass_instr(needs_super, &v.instr, ver);

  VarBlock {
    vars: v
      .vars
      .iter()
      .map(|v| match v {
        decomp::VarKind::This => VarKind::This,
        decomp::VarKind::Arg(_) => VarKind::Arg,
        decomp::VarKind::Local => VarKind::Local,
      })
      .collect(),
    block,
  }
}

fn pass_instr(needs_super: bool, p: &[decomp::Instr], ver: Version) -> Vec<Instr> {
  let mut res = Vec::with_capacity(p.len());
  for (_, instr) in p.iter().enumerate() {
    match instr.clone() {
      decomp::Instr::Loop(cond, body) => {
        if let Some(Instr::Let(_, Expr { initial: _, ops })) = res.last() {
          if ops.len() == 1 && ops[0] == Op::Add(Expr::lit(1)) {
            if let decomp::Cond::LT(lhs, rhs) = cond {
              res.pop(); // Remove the Let
              res.push(Instr::For(
                Expr::from_decomp(lhs, ver).unwrap_initial().unwrap_var(),
                Range { min: Expr::lit(0), max: Expr::from_decomp(rhs.clone(), ver) },
                pass_instr(needs_super, &body, ver),
              ));
            } else {
              panic!("unknown conditional while parsing function");
            }
          } else {
            panic!("unknown loop syntax while parsing function");
          }
        }
      }
      decomp::Instr::If(cond, when_true, when_false) => res.push(Instr::If(
        Cond::from_decomp(cond, ver),
        pass_instr(needs_super, &when_true, ver),
        pass_instr(needs_super, &when_false, ver),
      )),
      decomp::Instr::Switch(val, blocks) => res.push(Instr::Switch(
        Expr::from_decomp(val, ver),
        blocks
          .into_iter()
          .map(|(key, block)| (key, pass_instr(needs_super, &block, ver)))
          .collect(),
      )),
      decomp::Instr::SetField(name, val) => res.push(Instr::Set(name, Expr::from_decomp(val, ver))),
      decomp::Instr::Assign(var, item) => res.push(Instr::Let(var, Expr::from_decomp(item, ver))),
      decomp::Instr::SetArr(var, idx, item) => res.push(Instr::SetArr(
        Expr::from_decomp(var, ver),
        Expr::from_decomp(idx, ver).unwrap_initial(),
        Expr::from_decomp(item, ver),
      )),
      decomp::Instr::Super(_) => {
        if needs_super {
          res.push(Instr::Super)
        }
      }
      decomp::Instr::Expr(item) => res.push(Instr::Expr(Expr::from_decomp(item, ver))),
      decomp::Instr::Return(v) => res.push(Instr::Return(Box::new(Expr::from_decomp(v, ver)))),
    }
  }
  res
}

impl Expr {
  pub fn lit(v: impl Into<Lit>) -> Self {
    Expr { initial: Value::Lit(v.into()), ops: vec![] }
  }
  pub fn new(v: impl Into<Value>) -> Self {
    Expr { initial: v.into(), ops: vec![] }
  }
}

impl Expr {
  fn from_decomp(v: decomp::Item, ver: Version) -> Self {
    let initial = Value::from_decomp(v.initial, ver);
    let ops = v.ops.borrow().clone();
    let mut expr = Expr { initial, ops: Vec::with_capacity(ops.len()) };
    ops.into_iter().for_each(|o| expr.add_op(o, ver));
    expr
  }
}

impl Value {
  fn from_decomp(v: decomp::Value, ver: Version) -> Self {
    match v {
      decomp::Value::Lit(v) => Value::Lit(Lit::Int(v)),
      decomp::Value::LitFloat(v) => Value::Lit(Lit::Float(v)),
      decomp::Value::String(v) => Value::Lit(Lit::String(v)),
      decomp::Value::Null => Value::Null,
      decomp::Value::Field(_, v) => Value::Field(v),
      decomp::Value::Array(len) => Value::Array(Box::new(Expr::from_decomp(*len, ver))),
      decomp::Value::StaticCall(class, name, args) => Value::CallStatic(
        class,
        name,
        args.into_iter().map(|a| Expr::from_decomp(a, ver)).collect(),
      ),
      decomp::Value::StaticField(class, _ty, name) => Value::Static(class, name),
      decomp::Value::Var(n) => Value::Var(n),
      decomp::Value::Class(name) => Value::New(name, vec![]),
      decomp::Value::MethodRef(class, name) => Value::MethodRef(class, name),
      decomp::Value::Closure(args, instr) => Value::Closure(
        args.into_iter().map(|a| Expr::from_decomp(a, ver)).collect(),
        pass(false, &instr, ver),
      ),
      _ => unimplemented!("value {:?}", v),
    }
  }
}
impl Expr {
  fn add_op(&mut self, op: decomp::Op, ver: Version) {
    self.ops.push(match op {
      decomp::Op::And(v) => Op::BitAnd(Expr::from_decomp(v, ver)),
      decomp::Op::Or(v) => Op::BitOr(Expr::from_decomp(v, ver)),
      decomp::Op::Add(v) => Op::Add(Expr::from_decomp(v, ver)),
      decomp::Op::Div(v) => Op::Div(Expr::from_decomp(v, ver)),
      decomp::Op::Mul(v) => Op::Mul(Expr::from_decomp(v, ver)),
      decomp::Op::Shr(v) => Op::Shr(Expr::from_decomp(v, ver)),
      decomp::Op::UShr(v) => Op::UShr(Expr::from_decomp(v, ver)),
      decomp::Op::Shl(v) => Op::Shl(Expr::from_decomp(v, ver)),
      decomp::Op::Len => Op::Len,
      decomp::Op::If(cond, new) => {
        Op::If(Box::new(Cond::from_decomp(cond, ver)), Expr::from_decomp(new, ver))
      }
      decomp::Op::Call(class, name, call_args) => {
        if name == "<init>" {
          assert!(self.ops.is_empty(), "cannot call constructor on value with ops {:?}", self);
          if let Value::New(_, args) = &mut self.initial {
            call_args.into_iter().for_each(|a| args.push(Expr::from_decomp(a, ver)));
            return;
          } else {
            panic!("cannot call constructor on non-class type {:?}", self);
          }
        } else {
          self.ops.push(Op::Call(
            class,
            name,
            call_args.into_iter().map(|v| Expr::from_decomp(v, ver)).collect(),
          ));
          return;
        }
      }
      decomp::Op::Get(item) => match Expr::from_decomp(item, ver).initial {
        // Value::Call(Some(v), name, args) => {
        //   if name == "ordinal" {
        //     assert!(args.is_empty(), "ordinal shouldn't have any args");
        //     assert!(self.ops.is_empty(), "ordinal shouldn't have any ops {:?}", self);
        //     *self = *v;
        //     return;
        //   } else {
        //     Op::Idx(Expr::new(v).op(Op::Call(name, args)))
        //   }
        // }
        Value::Var(v) => Op::Idx(Expr::new(Value::Var(v))),
        Value::Field(name) => Op::Field(name),
        v => panic!("unexpected Get {:?}", v),
      },
      decomp::Op::Cast(ty) => Op::Cast(ty),
      _ => unimplemented!("op {:?}", op),
    });
  }
}
impl Cond {
  fn from_decomp(v: decomp::Cond, ver: Version) -> Self {
    match v {
      decomp::Cond::EQ(lhs, rhs) => {
        Cond::Eq(Expr::from_decomp(lhs, ver), Expr::from_decomp(rhs, ver))
      }
      decomp::Cond::NE(lhs, rhs) => {
        Cond::Neq(Expr::from_decomp(lhs, ver), Expr::from_decomp(rhs, ver))
      }
      decomp::Cond::LT(lhs, rhs) => {
        Cond::Less(Expr::from_decomp(lhs, ver), Expr::from_decomp(rhs, ver))
      }
      decomp::Cond::GT(lhs, rhs) => {
        Cond::Greater(Expr::from_decomp(lhs, ver), Expr::from_decomp(rhs, ver))
      }
      decomp::Cond::LE(lhs, rhs) => {
        Cond::Lte(Expr::from_decomp(lhs, ver), Expr::from_decomp(rhs, ver))
      }
      decomp::Cond::GE(lhs, rhs) => {
        Cond::Gte(Expr::from_decomp(lhs, ver), Expr::from_decomp(rhs, ver))
      }
      decomp::Cond::Or(lhs, rhs) => {
        Cond::Or(Box::new(Cond::from_decomp(*lhs, ver)), Box::new(Cond::from_decomp(*rhs, ver)))
      }
    }
  }
}

impl From<i32> for Lit {
  fn from(v: i32) -> Self {
    Lit::Int(v)
  }
}
impl From<f32> for Lit {
  fn from(v: f32) -> Self {
    Lit::Float(v)
  }
}

impl Expr {
  #[track_caller]
  pub fn unwrap_initial(self) -> Value {
    assert!(self.ops.is_empty(), "cannot unwrap initial when operators are present");
    self.initial
  }
}
impl Value {
  #[track_caller]
  pub fn unwrap_var(self) -> usize {
    match self {
      Value::Var(v) => v,
      _ => panic!("not a variable: {:?}", self),
    }
  }
  #[track_caller]
  #[allow(unused)]
  pub fn unwrap_lit(self) -> Lit {
    match self {
      Value::Lit(v) => v,
      _ => panic!("not a literal: {:?}", self),
    }
  }
  #[track_caller]
  #[allow(unused)]
  pub fn unwrap_call_static(self) -> (String, String, Vec<Expr>) {
    match self {
      Value::CallStatic(class, name, args) => (class, name, args),
      _ => panic!("not a static call: {:?}", self),
    }
  }
}
impl Lit {
  #[track_caller]
  #[allow(unused)]
  pub fn unwrap_int(self) -> i32 {
    match self {
      Lit::Int(v) => v,
      _ => panic!("not an int: {:?}", self),
    }
  }
}
