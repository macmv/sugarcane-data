import commands

def main():
  side = commands.CLIENT
  keep_lvt = True
  keep_generics = True

  c = commands.Commands()
  print('> Creating SRGs')
  c.createsrgs(side, use_srg=True)
  print('> Applying SpecialSource')
  c.applyss(side, keep_lvt=keep_lvt, keep_generics=keep_generics)

if __name__ == '__main__':
    main()
