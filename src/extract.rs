use super::Version;
use crate::download;
use std::{fs, io, path::PathBuf};

pub fn mcp(url: &str, path: &str) -> io::Result<()> {
  let p: PathBuf = path.into();
  info!("DOWNLOADING mcp to {}", path);

  fs::create_dir_all(&p)?;
  if download::download_file(url, &p.join("mcp.zip"), None)? {
    download::unzip(&p.join("mcp.zip"), &p.join("mcp"))?;
  }

  if path.ends_with("mc-12-02") {
    let changed = download::download_file(
      "http://files.minecraftforge.net/maven/de/oceanlabs/mcp/mcp/1.12.2/mcp-1.12.2-srg.zip",
      &p.join("srg.zip"),
      None,
    )?;
    if changed {
      download::unzip(&p.join("srg.zip"), &p.join("mcp/conf"))?;
    }
  }

  Ok(())
}

pub fn yarn(path: &str, ver: Version) -> io::Result<()> {
  let p: PathBuf = path.into();
  info!("DOWNLOADING yarn to {}", path);

  fs::create_dir_all(&p)?;
  if download::download_file(
    &format!("https://github.com/FabricMC/yarn/archive/refs/heads/{}.zip", ver),
    &p.join("yarn.zip"),
    None,
  )? {
    download::unzip(&p.join("yarn.zip"), &p)?;
    if p.join("yarn").exists() {
      fs::remove_dir_all(p.join("yarn"))?;
    }
    fs::rename(p.join(format!("yarn-{}", ver)), p.join("yarn")).unwrap();
  }
  download::download_file(
    &format!("https://github.com/FabricMC/intermediary/raw/master/mappings/{}.tiny", ver),
    &p.join("intermediary.tiny"),
    None,
  )?;

  Ok(())
}
