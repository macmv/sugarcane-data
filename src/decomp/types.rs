use super::{Descriptor, Type};
use std::{cell::RefCell, rc::Rc};

/// The body of a function or closure. This includes a table of all variables to
/// their kind. This is what maps the variable ids to either `this`, an
/// argument, or a local variable.
#[derive(Debug, Clone, PartialEq)]
pub struct VarBlock {
  pub vars:  Vec<VarKind>,
  pub instr: Vec<Instr>,
}

/// The kind of variable this is.
#[derive(Debug, Clone, PartialEq)]
pub enum VarKind {
  This,
  Arg(Type),
  Local,
}

#[derive(Debug, Clone, PartialEq)]
pub enum Value {
  /// Used for the second item when storing a 64 bit item on the stack. Each
  /// stack frame is 32 bits, but in our implementation, it is much easier to
  /// just store the 64 bit value in one stack frame. In order for indexes to
  /// still work, we need a placeholder in the stack, and that is what this is
  /// used for.
  Partial64,
  /// Used when we have a IfNull instruction.
  Null,
  /// A literal value
  Lit(i32),
  /// A literal float
  LitFloat(f32),
  /// A reference to a local variable. These are parsed out to some degree, but
  /// when they are present, it generally means that a value was read from the
  /// buffer into a variable, and is then used multiple times.
  Var(usize),
  /// An object field
  Field(Box<Item>, String),
  /// A constant string
  String(String),
  /// A static function call. The first item is the class name, the second is
  /// the function name, and the third are the arguments to the function.
  StaticCall(String, String, Vec<Item>),
  /// A static field of some class. The first element is the class, second is
  /// the type, and third is the name of the field.
  StaticField(String, Type, String),
  /// A method name on a class. This is essentially a function pointer.
  MethodRef(String, String),
  /// Calls a closure. The first list are the arguments to that closure, and the
  /// second list is the closure contents.
  Closure(Vec<Item>, VarBlock),
  /// An array with a (possibly constant) length
  Array(Box<Item>),
  /// A class instance
  Class(String),

  /// The value we store in the local variables array at index 0. Will never
  /// appear on the stack.
  This,
  /// The value we store at index 1 in the local variables array. Will never
  /// appear on the stack.
  Buf,
}

#[derive(Debug, Clone, PartialEq)]
pub enum Instr {
  /// Used to call the super class. This only applies to some packets, whose
  /// fields extend another packet.
  Super(Vec<Item>),
  /// Used whenever we assign to a variable in the java variables list (a local
  /// variable). This typically translates to a variable that will be used in
  /// multiple SetField calls.
  Assign(usize, Item),
  /// Used whenever we set a field in the output. The item may be a direct
  /// buffer read, or it may be a variable lookup.
  SetField(String, Item),
  /// Used when setting in an array. The first item is the array, the second is
  /// the index, and the last is the value.
  SetArr(Item, Item, Item),
  /// Used when we need to ignore the result of an expression.
  Expr(Item),
  /// An if statement. The conditional should be checked once. If true, the
  /// first set of instructions should be executed. If false, the other set of
  /// instructions should be executed.
  If(Cond, Vec<Instr>, Vec<Instr>),
  /// A loop. The conditional should be checked once before every iteration. If
  /// true, all instructions within the loop should be executed in order.
  Loop(Cond, Vec<Instr>),
  /// A switch statement. Based on the value of item, the given index should be
  /// chosen out of the instructions map.
  Switch(Item, Vec<(i32, Vec<Instr>)>),
  /// Returns the given value.
  Return(Item),
}

#[derive(Debug, Clone, PartialEq)]
pub enum Op {
  /// &
  And(Item),
  /// |
  Or(Item),
  /// <<
  Shl(Item),
  /// >>
  Shr(Item),
  /// >>>
  UShr(Item),

  /// +
  Add(Item),
  /// -
  Sub(Item),
  /// *
  Mul(Item),
  /// /
  Div(Item),
  /// %
  Rem(Item),

  /// Negate
  Neg,
  /// Compare self with the given item. If self is less, then set self to -1. If
  /// self is greater, set self to 1. If self and other are the same, set self
  /// to 0.
  Cmp(Item),

  /// If cond is true, then replace self with item.
  If(Cond, Item),

  /// Array length
  Len,
  /// Set in array
  Set(Item, Item),
  /// Get in array
  Get(Item),

  /// Used any time we call a function on a value.
  Call(String, String, Vec<Item>),

  /// Casts the value to the given type. This is only used for numbers, and type
  /// casts are usually not needed in the disassembly.
  Cast(Type),
}

#[derive(Debug, Clone, PartialEq)]
pub enum Cond {
  /// Less
  LT(Item, Item),
  /// Greater
  GT(Item, Item),
  /// Less than or equal to
  LE(Item, Item),
  /// Greater than or eaual to
  GE(Item, Item),
  /// Equal
  EQ(Item, Item),
  /// Not equal
  NE(Item, Item),

  /// Either must be true
  Or(Box<Cond>, Box<Cond>),
}

#[derive(Debug, Clone, PartialEq)]
pub(super) struct Stack {
  pub items: Vec<Item>,
}

#[derive(Debug, Clone, PartialEq)]
pub struct Vars {
  pub items: Vec<Item>,
}

/// An item on the stack, that has been modified. This is how we track
/// mathematical operations performed on fields/variables.
#[derive(Clone, PartialEq)]
pub struct Item {
  pub initial: Value,
  pub ops:     Rc<RefCell<Vec<Op>>>,
}

#[derive(Debug, Clone, PartialEq)]
pub(super) enum Statement {
  /// A if or while statement. We don't kow which one until we hit the else, or
  /// the end of the loop.
  Cond {
    /// The conditional
    cond:        Cond,
    /// Where the initial jmp goes to. This is either the end of the statement,
    /// or the start of the else block.
    jmp:         u32,
    /// The instructions in the main block of the if statement, or the body of a
    /// loop.
    when_true:   Vec<Instr>,
    /// The instructions in the else block of the if statement. Empty if this is
    /// a loop.
    when_false:  Vec<Instr>,
    /// If we see a goto, this is set to true. The jmp location is updated, as
    /// we now know the original jmp was just pointing to the start of the
    /// else block.
    ///
    /// If the goto is jumping backwards, we don't set this, as we just reached
    /// the end of a loop.
    in_else:     bool,
    /// The stack if the conditional was true. This is a clone of the stack
    /// before the true block was entered.
    stack_true:  Stack,
    /// The stack if the conditional was false. This is a clone of the stack
    /// before the true block was entered.
    stack_false: Stack,
  },
  /// A switch statement. In java, there is a unique instruction for the start
  /// of a switch statement, which lays out a table of all possible jump
  /// locations. That is stored in this variant.
  Switch {
    /// The item to switch against
    val:   Item,
    /// Each element is a (key, offset, instr) pair.
    table: Vec<(i32, i32, Vec<Instr>)>,
    /// Current entry into table
    curr:  usize,
    /// Current stack. Cleared each time we hit a goto.
    stack: Stack,
    /// If we have seen a goto, this will be Some(idx). If we see multiple
    /// gotos, they must point to the same index. If we haven't seen any
    /// gotos yet, this will be None.
    end:   Option<u32>,
  },
}

#[derive(Debug, Clone, PartialEq)]
pub(super) struct State {
  /// The outermost stack. When in statements, the statement stores an inner
  /// stack. Once the statement ends, we resolve that stack into this one.
  pub stack:      Stack,
  /// Each index is a nested statement. These are used to track partial for/if
  /// blocks. Once finished parsing, this will be empty, and is considered
  /// invalid instructions if it is not empty.
  pub statements: Vec<Statement>,
  /// The output instructions.
  pub instr:      Vec<Instr>,
}

impl Instr {
  #[track_caller]
  pub fn unwrap_return(self) -> Item {
    match self {
      Self::Return(v) => v,
      _ => panic!("not a return: {:?}", self),
    }
  }
}

impl Item {
  #[track_caller]
  pub fn unwrap_initial(self) -> Value {
    assert!(self.ops.borrow().is_empty(), "cannot unwrap initial for non-empty ops");
    self.initial
  }
}

impl Op {
  #[track_caller]
  pub fn unwrap_call(self) -> (String, String, Vec<Item>) {
    match self {
      Op::Call(class, name, args) => (class, name, args),
      _ => panic!("not a call: {:?}", self),
    }
  }
  #[track_caller]
  pub fn unwrap_set(self) -> (Item, Item) {
    match self {
      Op::Set(index, val) => (index, val),
      _ => panic!("not a set: {:?}", self),
    }
  }
}
impl Value {
  #[track_caller]
  pub fn unwrap_int(self) -> i32 {
    match self {
      Value::Lit(v) => v,
      _ => panic!("not an int literal: {:?}", self),
    }
  }
  #[track_caller]
  pub fn unwrap_float(self) -> f32 {
    match self {
      Value::LitFloat(v) => v,
      _ => panic!("not a float literal: {:?}", self),
    }
  }
  #[track_caller]
  pub fn unwrap_string(self) -> String {
    match self {
      Value::String(v) => v,
      _ => panic!("not a string literal: {:?}", self),
    }
  }
  #[track_caller]
  pub fn unwrap_static_call(self) -> (String, String, Vec<Item>) {
    match self {
      Value::StaticCall(class, name, args) => (class, name, args),
      _ => panic!("not a static call: {:?}", self),
    }
  }
  #[track_caller]
  pub fn unwrap_class(self) -> String {
    match self {
      Value::Class(v) => v,
      _ => panic!("not a class: {:?}", self),
    }
  }
  #[track_caller]
  pub fn unwrap_static_field(self) -> (String, Type, String) {
    match self {
      Value::StaticField(class, ty, val) => (class, ty, val),
      _ => panic!("not a static field: {:?}", self),
    }
  }
}

impl VarBlock {
  pub fn new(stat: bool, desc: Descriptor, max_var: usize, instr: Vec<Instr>) -> Self {
    let mut vars = vec![];
    if !stat {
      vars.push(VarKind::This);
    }
    for a in desc.args {
      vars.push(VarKind::Arg(a));
    }
    for _ in vars.len()..max_var {
      vars.push(VarKind::Local);
    }
    VarBlock { vars, instr }
  }
}
