#![allow(clippy::single_match, clippy::too_many_arguments)]

#[macro_use]
extern crate log;

use ansi_term::Color;
use pprof::protos::Message;
use rayon::prelude::*;
use std::{fmt, fs, fs::File, io, io::Write, path::PathBuf, time::Instant};
use structopt::StructOpt;

mod analyzer;
mod decomp;
mod download;
mod extract;
mod version;
mod web;

#[derive(StructOpt, Debug)]
struct Cli {
  /// The build directory. All generated output will be placed within this
  /// directory.
  #[structopt(short, long, default_value = "build")]
  build: String,

  /// If set, downloading the minecraft versions will be skipped. If not already
  /// downloaded, this may cause things to break.
  #[structopt(long)]
  no_download: bool,
  /// If set, the mcp/yarn extraction will be skipped, and the program will
  /// assume that all class files have already been extracted.
  #[structopt(long)]
  no_extract:  bool,
  /// If set, the class files will not be analyzed. Setting this along with the
  /// other no_* flags will disable the entire program. This is here so that I
  /// can develop the webpage without analyzing everything every time.
  #[structopt(long)]
  no_analyze:  bool,

  /// A list of major versions to process. If left empty, all versions will be
  /// processed.
  #[structopt(short, long)]
  versions: Vec<u32>,

  /// If set, a webpage will be generated in `build/webpage`. This is disabled
  /// by default, as it is only used for the gitlab page.
  #[structopt(short, long)]
  webpage: bool,
  /// If set, all json outputs will be generated with the 'pretty' formatting.
  /// This will indent nested structures with 2 spaces, instead of just placing
  /// everything on one line.
  #[structopt(short, long)]
  pretty:  bool,

  /// If set, the logger will print line numbers.
  #[structopt(short, long)]
  lines:   bool,
  /// If set, a profiler will run, and generate `pprof.pb`, which can be opened
  /// like so: `go tool pprof -http 0.0.0.0:6060 pprof.pb`
  #[structopt(long)]
  profile: bool,
}

fn main() {
  let args = Cli::from_args();
  let guard = if args.profile {
    info!("starting cpu profiler");
    Some(pprof::ProfilerGuard::new(100).unwrap())
  } else {
    None
  };
  match run(args) {
    Ok(_) => {}
    Err(e) => {
      error!("{}", e);
    }
  }
  if let Some(guard) = guard {
    match guard.report().build() {
      Ok(report) => {
        let mut file = File::create("pprof.pb").unwrap();
        let profile = report.pprof().unwrap();

        let mut content = Vec::new();
        profile.encode(&mut content).unwrap();
        file.write_all(&content).unwrap();
      }
      Err(e) => {
        error!("failed to generate report: {}", e);
      }
    };
  }
}

/// A version. The internal value is the protocol version.
#[derive(Debug, Clone, Copy, PartialEq, Eq, PartialOrd, Ord)]
pub struct Version {
  maj: u32,
  min: u32,
}

fn run(args: Cli) -> io::Result<()> {
  fs::create_dir_all(&args.build)?;
  init(args.lines);

  let build = PathBuf::new().join(args.build);

  if !args.no_download {
    info!("> DOWNLOADING minecraft versions");
    version::download_all(&build)?;
    info!("> FINISHED downloading minecraft versions");
  }

  if !args.no_extract {
    info!("> DOWNLOADING mcp and yarn");
    extract::mcp("http://www.modcoderpack.com/files/mcp918.zip", "build/mc-08-09")?;
    extract::mcp("http://www.modcoderpack.com/files/mcp928.zip", "build/mc-09-04")?;
    extract::mcp("http://www.modcoderpack.com/files/mcp931.zip", "build/mc-10-02")?;
    extract::mcp("http://www.modcoderpack.com/files/mcp937.zip", "build/mc-11-02")?;
    extract::mcp("http://www.modcoderpack.com/files/mcp940.zip", "build/mc-12-02")?;

    for version in VERSIONS {
      if version.maj() < 14 {
        continue;
      }
      extract::yarn(&format!("build/{}", version.build_dir()), *version)?;
    }

    info!("> FINISHED downloading mcp and yarn");
  }

  let versions = if args.versions.is_empty() {
    VERSIONS.to_vec()
  } else {
    args
      .versions
      .iter()
      .flat_map(|num| match Version::from_num(*num) {
        Some(v) => Some(v),
        None => {
          warn!("unknown version {num}, skipping");
          None
        }
      })
      .collect()
  };

  if !args.no_analyze {
    info!("> ANALYZING versions");
    let outputs: Vec<_> = versions
      .par_iter()
      .flat_map(|&ver| match analyzer::analyze(&build, ver, args.pretty) {
        Ok(out) => Some((ver, out)),
        Err(e) => {
          error!("could not process version {ver}: {:?}", e);
          None
        }
      })
      .collect();
    let time = Instant::now();
    info!("writing versions to disk...");
    for (ver, out) in outputs {
      match out.to_disk() {
        Ok(()) => {}
        Err(e) => error!("could not write version {ver} to disk: {:?}", e),
      }
    }
    info!("writing versions to disk took {:?}", Instant::now().duration_since(time));
    info!("> FINISHED analyzing versions");
  }

  if args.webpage {
    info!("> GENERATING webpage");
    web::generate(&build, &versions)?;
    info!("> FINISHED generating webpage");
  }

  Ok(())
}

pub static VERSIONS: &[Version] = &[
  Version::new(8, 9),
  Version::new(9, 4),
  Version::new(10, 2),
  Version::new(11, 2),
  Version::new(12, 2),
  Version::new(14, 4),
  Version::new(15, 2),
  Version::new(16, 5),
  Version::new(17, 1),
  Version::new(18, 0),
  Version::new(18, 2),
];

impl Version {
  pub const fn from_num(num: u32) -> Option<Self> {
    Some(match num {
      8 => Version::new(8, 9),
      9 => Version::new(9, 4),
      10 => Version::new(10, 2),
      11 => Version::new(11, 2),
      12 => Version::new(12, 2),
      14 => Version::new(14, 4),
      15 => Version::new(15, 2),
      16 => Version::new(16, 5),
      17 => Version::new(17, 1),
      18 => Version::new(18, 2),
      _ => return None,
    })
  }
  pub const fn new(maj: u32, min: u32) -> Self {
    Version { maj, min }
  }

  pub fn maj(&self) -> u32 {
    self.maj
  }
  pub fn min(&self) -> u32 {
    self.min
  }
  pub fn is_old(&self) -> bool {
    self.maj < 13
  }

  pub fn build_dir(&self) -> String {
    format!("mc-{:02}-{:02}", self.maj(), self.min())
  }
}

impl fmt::Display for Version {
  fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
    if self.min() != 0 {
      write!(f, "1.{}.{}", self.maj(), self.min())
    } else {
      write!(f, "1.{}", self.maj())
    }
  }
}

/// Initializes logger
pub fn init(show_line: bool) {
  fern::Dispatch::new()
    .format(move |out, message, record| {
      out.finish(format_args!(
        "{}{} [{}] {}",
        chrono::Local::now().format("%Y-%m-%d %H:%M:%S:%f"),
        {
          if show_line {
            format!(" {}:{}", record.file().unwrap(), record.line().unwrap())
          } else {
            "".into()
          }
        },
        match record.level() {
          l @ log::Level::Error => Color::Red.bold().paint(l.to_string()),
          l @ log::Level::Warn => Color::Yellow.paint(l.to_string()),
          l @ log::Level::Info => Color::Green.paint(l.to_string()),
          l @ log::Level::Debug => Color::Blue.paint(l.to_string()),
          l => Color::White.paint(l.to_string()),
        },
        message
      ))
    })
    .level(log::LevelFilter::Debug)
    .level_for("ureq", log::LevelFilter::Info)
    .level_for("rustls", log::LevelFilter::Info)
    .chain(std::io::stdout())
    .chain(fern::log_file("build/build.log").unwrap())
    .apply()
    .unwrap();
}
