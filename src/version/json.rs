//! This is the json data that mojang sends us when downloading versions. We
//! allow dead code, so that the structs here match the mojang data.

use crate::Version;
use serde::Deserialize;
use std::{collections::HashMap, io};

#[derive(Debug, Deserialize)]
#[allow(dead_code)]
pub struct Manifest {
  latest:   LatestVersion,
  versions: Vec<ManifestVersion>,
}

#[derive(Debug, Deserialize)]
#[allow(dead_code)]
struct LatestVersion {
  release:  String,
  snapshot: String,
}

#[derive(Debug, Deserialize)]
#[allow(dead_code)]
struct ManifestVersion {
  id:           String,
  #[serde(rename = "type")]
  ty:           String,
  url:          String,
  time:         String,
  #[serde(rename = "releaseTime")]
  release_time: String,
}

#[derive(Debug, Deserialize)]
pub struct VersionInfo {
  pub arguments:                Option<Args>,
  #[serde(rename = "assetIndex")]
  pub asset_index:              AssetIndex,
  pub assets:                   String,
  pub downloads:                Downloads,
  pub id:                       String,
  pub libraries:                Vec<Lib>,
  pub logging:                  Logging,
  pub main_class:               Option<String>,
  #[serde(rename = "minimumLauncherVersion")]
  pub minimum_launcher_version: i32,
  #[serde(rename = "releaseTime")]
  pub release_time:             String,
  pub time:                     String,
  #[serde(rename = "type")]
  pub ty:                       String,
}

#[derive(Debug, Deserialize)]
pub struct Args {
  pub game: Vec<GameArg>,
  pub jvm:  Vec<JvmArg>,
}

#[derive(Debug, Deserialize)]
#[serde(untagged)]
pub enum GameArg {
  Literal(String),
  Rule { rules: Vec<GameArgRule>, value: GameArgValue },
}

#[derive(Debug, Deserialize)]
pub struct GameArgRule {
  pub action:   String,
  pub features: HashMap<String, bool>,
}

#[derive(Debug, Deserialize)]
#[serde(untagged)]
pub enum GameArgValue {
  Literal(String),
  List(Vec<String>),
}

#[derive(Debug, Deserialize)]
#[serde(untagged)]
pub enum JvmArg {
  Literal(String),
  Rule { rules: Vec<JvmArgRule>, value: JvmArgValue },
}

#[derive(Debug, Deserialize)]
pub struct JvmArgRule {
  pub action: String,
  pub os:     HashMap<String, String>,
}

#[derive(Debug, Deserialize)]
#[serde(untagged)]
pub enum JvmArgValue {
  Literal(String),
  List(Vec<String>),
}

#[derive(Debug, Deserialize)]
pub struct AssetIndex {
  pub id:         String,
  pub sha1:       String,
  pub size:       u64,
  #[serde(rename = "totalSize")]
  pub total_size: u64,
  pub url:        String, // This is versions/1.a.b/1.a.b.json
}

#[derive(Debug, Deserialize)]
pub struct Downloads {
  pub client:          Download,
  pub client_mappings: Option<Download>,
  pub server:          Download,
  pub server_mappings: Option<Download>,
}

#[derive(Debug, Deserialize)]
pub struct Download {
  pub sha1: String,
  pub size: u64,
  pub url:  String, // This is versions/1.a.b/1.a.b.jar, and it is what we decompile
}

#[derive(Debug, Deserialize)]
pub struct Lib {
  pub downloads: LibDownload,
  pub name:      String,
  pub natives:   Option<HashMap<String, String>>,
  pub rules:     Option<Vec<LibRule>>,
}

#[derive(Debug, Deserialize)]
pub struct LibDownload {
  pub artifact:    Option<LibArtifact>,
  pub classifiers: Option<HashMap<String, LibArtifact>>,
}

#[derive(Debug, Deserialize)]
pub struct LibArtifact {
  pub path: String,
  pub sha1: String,
  pub size: u64,
  pub url:  String, // This is something like lwjgl.jar
}

#[derive(Debug, Deserialize)]
pub struct LibRule {
  pub action: String,
  pub os:     Option<HashMap<String, String>>,
}

#[derive(Debug, Deserialize)]
pub struct Logging {
  pub client: LoggingClient,
}

#[derive(Debug, Deserialize)]
pub struct LoggingClient {
  pub argument: String,
  pub file:     LoggingFile,
  #[serde(rename = "type")]
  pub ty:       String,
}

#[derive(Debug, Deserialize)]
pub struct LoggingFile {
  pub id:   String,
  pub sha1: String,
  pub size: u64,
  pub url:  String,
}

/// Returned tuple is version id (1.8.8), then the url of the version json, then
/// the decoded json.
pub fn download_all() -> io::Result<Vec<(Version, String, VersionInfo)>> {
  let manifest: Manifest =
    ureq::get("https://launchermeta.mojang.com/mc/game/version_manifest.json")
      .call()
      .map_err(|e| io::Error::new(io::ErrorKind::InvalidData, e.to_string()))?
      .into_json()
      .map_err(|e| io::Error::new(io::ErrorKind::InvalidData, e.to_string()))?;

  let versions_map: HashMap<_, _> =
    manifest.versions.into_iter().map(|v| (v.id.clone(), v)).collect();

  let mut out = vec![];

  for v in super::super::VERSIONS {
    let ver = v.to_string();
    out.push((
      *v,
      versions_map[&ver].url.clone(),
      ureq::get(versions_map[&ver].url.as_str())
        .call()
        .map_err(|e| io::Error::new(io::ErrorKind::InvalidData, e.to_string()))?
        .into_json()
        .map_err(|e| io::Error::new(io::ErrorKind::InvalidData, e.to_string()))?,
    ));
  }

  Ok(out)
}
