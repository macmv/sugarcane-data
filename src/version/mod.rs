use crate::{download, Version};
use std::{
  fs, io,
  path::{Path, PathBuf},
};

mod json;
use json::VersionInfo;

pub fn download_all(build: &Path) -> io::Result<()> {
  let versions = json::download_all()?;

  for (ver, _ver_url, info) in versions {
    info!("DOWNLOADING minecraft version {}", ver);

    let path = build.join(ver.build_dir());
    fs::create_dir_all(path.join("mc"))?;
    download::download_file(
      &info.downloads.client.url,
      &path.join("mc/mc.jar"),
      Some(info.downloads.client.size),
    )?;
    // Unzipping this is nice, but it creates a single directory with thousands
    // of files. It ends up being much faster to just open the jar as a zip when
    // we decompile the thing.
    // download::unzip(&path.join("mc/mc.jar"), &path.join("src"))?;

    // Downloads assets info. Might be useful if we need to download the vanilla
    // texture pack.
    //
    // let path = PathBuf::new()
    //   .join("build")
    //   .join(ver_path(ver))
    //   .join(format!("extracted/jars/versions/{}/{}.json", ver, ver));
    // fs::create_dir_all(path.parent().unwrap())?;
    // download::download_file(&ver_url, &path, None)?;

    // download_libs(ver, &info)?;
  }

  Ok(())
}

/// Downloads all libraries. This is used when running mcp, which we don't
/// need to run. This is left here, as the logic is complex and annoying to
/// rewrite, and we might want to do a full decompile sometime.
#[allow(dead_code)]
fn download_libs(ver: Version, info: &VersionInfo) -> io::Result<()> {
  for lib in &info.libraries {
    if let Some(rules) = &lib.rules {
      let mut valid = false;
      for r in rules {
        match r.action.as_str() {
          "allow" => {
            if let Some(os) = &r.os {
              if os["name"] == "linux" {
                valid = true;
              }
            } else {
              valid = true;
            }
          }
          "disallow" => {
            if let Some(os) = &r.os {
              if os["name"] == "linux" {
                valid = false;
              }
            } else {
              valid = false;
            }
          }
          v => panic!("invalid action {}", v),
        }
      }
      if !valid {
        continue;
      }
    }
    if let Some(natives) = &lib.natives {
      if let Some(name) = natives.get("linux") {
        let artifact = &lib.downloads.classifiers.as_ref().unwrap()[name];
        let path = PathBuf::new()
          .join("build")
          .join(ver.build_dir())
          .join(format!("extracted/jars/versions/{}/{}-natives/{}", ver, ver, artifact.path));
        fs::create_dir_all(path.parent().unwrap())?;
        download::download_file(&artifact.url, &path, Some(artifact.size))?;
      }
    }
    if let Some(artifact) = &lib.downloads.artifact {
      let path = PathBuf::new()
        .join("build")
        .join(ver.build_dir())
        .join(format!("extracted/jars/libraries/{}", artifact.path));
      fs::create_dir_all(path.parent().unwrap())?;
      download::download_file(&artifact.url, &path, Some(artifact.size))?;
    }
  }
  Ok(())
}
