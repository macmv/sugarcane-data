use super::Version;
use html_builder::{Buffer, Html5, Node};
use std::{fmt, fmt::Write, fs, io, path::Path};

struct Webpage {
  rows: Vec<String>,

  protocol:  Vec<String>,
  blocks:    Vec<String>,
  items:     Vec<String>,
  entities:  Vec<String>,
  particles: Vec<String>,
  tags:      Vec<String>,
}

pub fn generate(build: &Path, versions: &[Version]) -> io::Result<()> {
  let w = build.join("webpage");
  fs::create_dir_all(&w)?;

  let mut protocol = vec![];
  let mut blocks = vec![];
  let mut items = vec![];
  let mut entities = vec![];
  let mut particles = vec![];
  let mut tags = vec![];

  macro_rules! copy {
    ($p:expr, $v:expr, [$($path:expr => $arr:expr,)*]) => {
      $(
        let name = format!(concat!($path, "-{}.json"), $v);
        fs::copy($p.join(concat!($path, ".json")), w.join(&name))?;
        $arr.push(name);
      )*
    };
  }

  for v in versions {
    let p = build.join(v.build_dir()).join("generated");
    copy!(p, v, [
      "protocol" => protocol,
      "blocks" => blocks,
      "items" => items,
      "entities" => entities,
      "particles" => particles,
      "tags" => tags,
    ]);
  }

  Webpage {
    rows: versions.iter().map(|v| v.to_string()).collect(),
    protocol,
    blocks,
    items,
    entities,
    particles,
    tags,
  }
  .generate(&w.join("index.html"))?;

  info!("webpage stored at {}", w.join("index.html").canonicalize().unwrap().to_str().unwrap());
  Ok(())
}

impl Webpage {
  pub fn generate(&self, path: &Path) -> io::Result<()> {
    let mut doc = Buffer::new();
    let mut html = doc.html().attr("lang='en'");
    self.write_head(&mut html.head()).unwrap();
    self.write_body(&mut html.body()).unwrap();

    fs::write(path, doc.finish())?;

    Ok(())
  }

  fn write_head(&self, head: &mut Node) -> fmt::Result {
    writeln!(head.title(), "Bamboo Data Downloads")?;
    writeln!(head.link().attr("rel='preconnect'").attr("href='https://fonts.googleapis.com'"))?;
    writeln!(head.link().attr("rel='preconnect'").attr("href='https://fonts.gstatic.com'"))?;
    writeln!(head
      .link()
      .attr("href='https://fonts.googleapis.com/css2?family=Noto+Sans&display=swap'")
      .attr("rel='stylesheet'"))?;
    writeln!(head.style(), "{}", include_str!("template/style.css"))?;

    Ok(())
  }
  fn write_body(&self, body: &mut Node) -> fmt::Result {
    let mut body = body.div().attr("class='main'");
    writeln!(body.h1(), "Bamboo Data Downloads")?;

    let mut tab = body.table();
    let columns = vec![
      ("Protocol", &self.protocol),
      ("Blocks", &self.blocks),
      ("Items", &self.items),
      ("Entities", &self.entities),
      ("Particles", &self.particles),
      ("Tags", &self.tags),
    ];

    let mut row = tab.tr();
    writeln!(row.th().attr("class='corner'"), "Version")?;
    for (name, _) in &columns {
      writeln!(row.th().attr("class='top'"), "{}", name)?;
    }
    drop(row);
    for (i, name) in self.rows.iter().enumerate() {
      let mut row = tab.tr();
      writeln!(row.th().attr("class='left'"), "{}", name)?;
      for (_, col) in &columns {
        let text = &col[i];
        writeln!(row.td().a().attr(&format!("href={}", text)), "{}", text)?;
      }
    }

    writeln!(body.footer(), "Generated from the <a href='https://gitlab.com/macmv/sugarcane-data'>Bamboo Data Generator</a>")?;

    Ok(())
  }
}
